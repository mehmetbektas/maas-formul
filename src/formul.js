//'use strict';

var parametreler = ['parametre1', 'parametre2', 'parametre3'];
var operatorler = ['+', '-', '*', '/', '(', ')', '.'];
var rakamlar = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
var kosulOperatorleri = ['>', '>=', '==', '!=', '<', '<='];

var arr = [];

$(document).ready(function () {
  for (var i = 0; i < operatorler.length; i++) {
    var element = $("<div></div>").text(operatorler[i]);
    $("#operator-area").append(element);
  }

  for (var i = 0; i < parametreler.length; i++) {
    var element = $("<div></div>").text(parametreler[i]);
    $("#parametre-area").append(element);
  }

  for (var i = 0; i < rakamlar.length; i++) {
    var element = $("<div></div>").text(rakamlar[i]);
    $("#rakam-area").append(element);
  }

});

$("#yeniDurumEkle").click(function () {
  var durumSayisi = $('#durumDiv table').length;
  var kosulSayisi = 0;
  //Yeni durum eklendiğinde, eklenen durum içindeki koşul sayısı 1 olacak, yani indexi 0 olacaktır.

  var yeniDurumHtmlTest = getYeniDurumHtml();
  
  yeniDurumHtmlTest += `
      <table id="kosulTableElse" style="width:30%">
        <tr>
          <td>
            <span style="padding-left:44px"><label>Else</label></span>            
          </td>
          <td>
            <textarea id="kosulFormuluElse"></textarea>
          </td>
          <td>
            <button id="kosulFormuluGetirElse" onclick="formuluGetir()">Formülü Getir</button>            
          </td>
          <td><button class="silButton" onclick="elseSil()">Sil</button></td>
        </tr>
      </table>
    `;
  if(durumSayisi > 0){
    $('#kosulTableElse').remove();
  }
  $("#durumDiv").append(yeniDurumHtmlTest);
  
  comboBoxDoldur(parametreler, 'comboParametre_' + durumSayisi + '_' + kosulSayisi);
  comboBoxDoldur(kosulOperatorleri, 'comboKosulOperatorleri_' + durumSayisi + '_' + kosulSayisi);
});

$("#formulGoster").click(function () {
  var formulArray = [];
  var searchEles = document.getElementById("formul-area").children;
  for (var i = 0; i < searchEles.length; i++) {
    if (searchEles[i].tagName == 'DIV') {
      formulArray.push(searchEles[i].innerText);
      var depth = 0;
      for (var j in formulArray) {
        console.log(formulArray[j]);
        console.log(typeof formulArray[j]);
        if (formulArray[j] == '(') {
          depth++;
        }
        if (formulArray[j] instanceof Number) {
          alert('thatsall');
        }
        else if (formulArray[j] == ')') {
          depth--;
        }
        if (depth < 0) text = "Input not valid";
      }
      if (depth > 0) text = "Input not valid"
      else text = "Input OK";
    }
  }
  console.log(text);
  alert('Formül: ' + formulArray);
  console.log('Formül: ' + formulArray);
});

comboBoxDoldur = function (itemList, comboBoxId) {
  var comboBox = document.getElementById(comboBoxId);
  for (var i = 0; i < itemList.length; i++) {
    var option = document.createElement('option');
    option.innerHTML = itemList[i];
    option.value = itemList[i];
    comboBox.appendChild(option);
  }
}

getYeniKosulHtml = function (index) {

  var indexArray = index.split('_');
  var durumIndex = indexArray[0];
  var kosulIndex = indexArray[1];
  
  var yeniKosulHtml = ``;

  if (kosulIndex == 0) {
    yeniKosulHtml += `
      <tr>
        <th><label>Parametre</label></th>
        <th><label>Operatör</label></th>
        <th><label>Değer</label></th>
        <th><label>Sil</label></th>
        <th><label></label></th>
        <th><label>Koşul Ekle</label></th>
        <th><label>Sonuç</label></th>
        <th><label>Formülü Getir</label></th>
      </tr>
    `;
    yeniKosulHtml = ``;
  }

  yeniKosulHtml += `
    <tr id="row_` + durumIndex + `">
  `;
  if(durumIndex == 0){
    yeniKosulHtml += `
      <td>
        <span style="padding-left:44px"><label>If</label></span>
      </td>
    `;
  }else if(durumIndex > 0){
    yeniKosulHtml += `
      <td>
        <label>ElseIf</label>
      </td>
    `;
  }
  yeniKosulHtml += `
      <td id="kosulTd_` + durumIndex +`">
        <div style="display: inline-flex;">
          <select style="width: 250px; height: 45px" id="comboParametre_` + durumIndex + `_` + kosulIndex + `"></select>
          <select style="height: 45px" id="comboKosulOperatorleri_` + durumIndex + `_` + kosulIndex + `"></select>
          <input style="width: 50px;height: 40px" id="kosulDegeri_` + durumIndex + `_` + kosulIndex + `" type="text"></input>
        </div>
      </td>
  `;

  if (kosulIndex == 0) {
    yeniKosulHtml += `
      <td id="yeniKosulEkleTd_` + durumIndex +`"><label onclick="yeniKosulEkle('` + durumIndex + `')">Koşul Ekle</label></td>
      <td id="kosulFormuluTd_` + durumIndex +`"><textarea id="kosulFormulu` + durumIndex + `_` + kosulIndex + `"></textarea></td>
      <td id="kosulFormuluGetirTd_` + durumIndex +`"><button id="kosulFormuluGetir` + durumIndex + `_` + kosulIndex + `" onclick="formuluSatiraGetir('` + durumIndex + `_` + kosulIndex + `')">Formülü Getir</button></td>
      <td><button class="silButton" onclick="durumuSil(` + durumIndex +`)">Sil</button></td>
    `;
  }

  yeniKosulHtml +=`
    </tr>
  `;

  return yeniKosulHtml;
}

getYeniDurumHtml = function () {
  var durumSayisi = $('#durumDiv table').length;
  var yeniDurumHtml = `
    <table id="kosulTable_`+ durumSayisi + `" style="width:40%;"> 
      `+ getYeniKosulHtml(durumSayisi+"_"+0) + `
    </table>
  `;
  return yeniDurumHtml; 
}

yeniKosulEkle = function (durumId) {
  var kosulSayisi = $('#kosulTable_'+durumId+' div').length;
  
  if(kosulSayisi == 1){
    var ekKosulHtml = ekKosulEkle(durumId,kosulSayisi);
    $('#kosulTd_'+durumId).append(ekKosulHtml);
    
    comboBoxDoldur(parametreler, 'comboParametre_' + durumId + '_' + kosulSayisi);
    comboBoxDoldur(kosulOperatorleri, 'comboKosulOperatorleri_' + durumId + '_' + kosulSayisi);
  }else if (kosulSayisi == 2) {
    alert("Şimdilik daha fazla koşul ekleyemezsiniz.");
    return;
  }
}

ekKosulEkle = function (durumIndex,kosulIndex) {
  var ekKosulHtml = `
    <div id="radioGroupCell_` + durumIndex + `" style="display: inline-flex;">
      <label for="radioButtonVeId">VE</label>
      <input type="radio" name="radioGroupVe_` + durumIndex + `_` + kosulIndex + `" value="&&"/>
      <label for="radioButtonVeyaId">VEYA</label>
      <input type="radio" name="radioGroupVeya_` + durumIndex + `_` + kosulIndex + `" value="||"/>
    </div>
    <div id="ikinciKosul_` + durumIndex +`"  style="display: inline-flex;">
      <select style="width: 250px; height: 45px" id="comboParametre_` + durumIndex + `_` + kosulIndex + `"></select>
      <select style="height: 45px" id="comboKosulOperatorleri_` + durumIndex + `_` + kosulIndex + `"></select>
      <input style="width: 50px;height: 40px" id="kosulDegeri_` + durumIndex + `_` + kosulIndex + `" type="text"></input>
      &nbsp;
      <button class="silButton" onclick="ikinciKosuluSil(` + durumIndex +`)">Sil</button>
    </div>
  `;
  return ekKosulHtml;
}

formuluAl = function () {
  var formulText = "";
  var searchEles = document.getElementById("formul-area").children;
  for (var i = 0; i < searchEles.length; i++) {
    if (searchEles[i].tagName == 'DIV') {
      formulText += searchEles[i].innerText;
    }
  }
  return formulText;
}

formulGecerliMi = function (formul) {
  if(!formul){
    return false;
  }else if(formul.length == 1){
    if(operatorler.includes(formul) ||  parametreler.includes(formul)){
      return false;
    }
  }else if(['+','-','*','/'].includes(formul[0])){
    return false;
  }else{
    return true;
  }
}

formuluSatiraGetir = function (id) {
  var formul = formuluAl();
  if(formulGecerliMi(formul)){
    $('#kosulFormulu'+id).val(formul);
  }else{
    alert("Formül geçersizdir. Düzeltip tekrar deneyiniz.");
  }
}

formuluGetir = function(){
  var formul = formuluAl();
  if(formulGecerliMi(formul)){
    $('#kosulFormuluElse').val(formul);
  }else{
    alert("Formül geçersizdir. Düzeltip tekrar deneyiniz.");
  }
}

durumuSil = function (durumId){
  $('#row_'+durumId).remove();
}
ikinciKosuluSil = function (durumId) {
  $('#radioGroupCell_'+durumId).remove();
  $('#ikinciKosul_'+durumId).remove(); 
}
elseSil = function(){
  $('#kosulTableElse').remove();
}

yeniParametreFormul = function () {
  var formul = "";
  formul = formuluAl();
  if(formulGecerliMi(formul)){
    $('#yeniParametreAlanı').val(formul);
  }else{
    alert("Formül geçersizdir. Düzeltip tekrar deneyiniz.");
  }
}

tumFormuluOlustur = function () {
  var durumSayisi = $('#durumDiv table').length;
  if(durumSayisi == 0){
    var formul = "";
    formul = formuluAl();
    if(formulGecerliMi(formul)){
      $('#formul').val(formul);
    }else{
      alert("Formül geçersizdir. Düzeltip tekrar deneyiniz.");
    }
  }else{
    var arr2 = [];
    var radio = [];
    var radioVeVal = "";
    var radioVeyaVal = "";    
    for(var durumIndex=0; durumIndex<durumSayisi; durumIndex++){
      var kosulSayisi = $('#kosulTable_'+durumIndex+' div').length;
      if(kosulSayisi > 1){
        kosulSayisi-=1;
      }
      var arr = [];
      for(var kosulIndex=0; kosulIndex<kosulSayisi; kosulIndex++){

        var item = [];      
        
        var q1 = $('#comboParametre_' + durumIndex + '_' + kosulIndex + ' option:selected ').text();
        var q2 = $('#comboKosulOperatorleri_' + durumIndex + '_' + kosulIndex + ' option:selected ').text();
        var q3 = $('#kosulDegeri_' + durumIndex + '_' + kosulIndex).val();
        var q4 = $('#kosulFormulu' + durumIndex + '_' + kosulIndex).val();

        item.push(q1);
        item.push(q2);
        item.push(q3);
        item.push(q4);

        arr.push(item);
        console.log(item.toString());
      }
      if(kosulSayisi != 0){
        arr2.push(arr);      
      }
      if(kosulSayisi>1){
        var radioVe = $("input[type='radio'][name='radioGroupVe_" + durumIndex + "_" + (kosulIndex-1) + "']:checked");
        var radioVeya = $("input[type='radio'][name='radioGroupVeya_" + durumIndex + "_" + (kosulIndex-1) + "']:checked");
        if(radioVe.length > 0){
          radioVeVal = radioVe.val();
          radioVeVal += " " + (arr2[durumIndex])[kosulSayisi-1][0] + " " + (arr2[durumIndex])[kosulSayisi-1][1] + " " + (arr2[durumIndex])[kosulSayisi-1][2];
          radio.push(radioVeVal);
        }else if(radioVeya.length > 0){
          radioVeyaVal = radioVeya.val();
          radioVeyaVal += " " + (arr2[durumIndex])[kosulSayisi-1][0] + " " + (arr2[durumIndex])[kosulSayisi-1][1] + " " + (arr2[durumIndex])[kosulSayisi-1][2];
          radio.push(radioVeyaVal);
        }
      }else{
        radio[durumIndex] = "";
      }
    }
    console.log(JSON.stringify(arr2));
    var kosulSayisi = $('#kosulTable_'+0+' div').length;
    var formul = "";
    for(var i=0; i<durumSayisi; i++){
      if($('#kosulTable_'+ i +' tr').length == 0){
        durumSayisi-=1;
      }
    }
    if(kosulSayisi == 1){
      formul = `
            var tutar=0.0;
            if(` + (arr2[0])[0][0] + ` ` + (arr2[0])[0][1] + ` ` + (arr2[0])[0][2] + `) {
              tutar=` + (arr2[0])[0][3] + `;
            }`;
    }else{
      formul = `
            var tutar=0.0;
            if(` + (arr2[0])[0][0] + ` ` + (arr2[0])[0][1] + ` ` + (arr2[0])[0][2] + ` ` + radio[0] + `) {
              tutar=` + (arr2[0])[0][3] + `;
            }`;
    }
    if(durumSayisi > 1){
      for(var durumIndex=1; durumIndex<durumSayisi; durumIndex++){
        var kosulSayisi = $('#kosulTable_'+durumIndex+' div').length;
        if(kosulSayisi > 1){
          formul += `elseIf(` + (arr2[durumIndex])[0][0] + ` ` + (arr2[durumIndex])[0][1] + ` ` + (arr2[durumIndex])[0][2] + ` ` + radio[durumIndex] + `) {
              tutar=` + (arr2[durumIndex])[0][3] + `;
            }`;
        }else{
          formul += `elseIf(` + (arr2[durumIndex])[0][0] + ` ` + (arr2[durumIndex])[0][1] + ` ` + (arr2[durumIndex])[0][2] + `) {
              tutar=` + (arr2[durumIndex])[0][3] + `;
            }`;
        }
        
      }
    }
    var tutarElse = document.getElementById("kosulFormuluElse").value;
    formul += `else{   
              tutar=` + tutarElse + `;
            }
    `;
    console.log(formul);
    $('#formul').append(formul);
  }
}

function split(val) {
  return val.split(/]\s*/);
}
function extractLast(term) {
  return split(term).pop();
}

$("#formul").textcomplete([{
    match: /(^|\b)(\w{2,})$/,
    search: function (term, callback) {
        callback($.map(parametreler, function (word) {
            return word.indexOf(term) === 0 ? word : null;
        }));
    },
    replace: function (word) {
        return word + ' ';
    }
}]);